warpshader = love.graphics.newShader([[
    #define P 0.0001 //derivative precision
    #define SIZE vec2(8, 60) //function size/precision
    #define WATER vec4(1) //water color

    float func(vec2 coords, float offset) {
      return (sin(coords.x - offset) * sin(coords.y + offset) + 1) / 10;
      //return (sin(coords.x - offset) + 1) / 2;
      //return cos(sqrt(pow(coords.x + offset, 2.0) + pow(coords.y + offset, 2.0)));
    }

    float xDerivative(vec2 coords, float offset) {
      return (func(coords + vec2(P, 0), offset) - func(coords - vec2(P, 0), offset))/((coords.x + P) - (coords.x - P));
    }

    float yDerivative(vec2 coords, float offset) {
      return (func(coords + vec2(0, P), offset) - func(coords - vec2(0, P), offset))/((coords.y + P) - (coords.y - P));
    }

    uniform float time = 0;
    uniform float xDisplacementScale = 2.0; // Scale for x displacement

    vec4 effect(vec4 color, Image image, vec2 texCoords, vec2 scrCoords) {
      float waveHeight = func(texCoords * SIZE, time);

      float xDeriv = xDerivative(texCoords * SIZE, time);
      float yDeriv = yDerivative(texCoords * SIZE, time);

      vec2 off;
      off.x = xDeriv * waveHeight / SIZE.x * xDisplacementScale; // Apply scale to x displacement
      off.y = yDeriv * waveHeight / SIZE.y;

      //return vec4(xDeriv, yDeriv, 0, 1);
      //return vec4(waveHeight, 0, 0, 1);
      return Texel(image, texCoords + off) * WATER;
    }
]])
