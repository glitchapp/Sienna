ASSETS = "new"	--	Available assets: New & Classic
require("slam")
require("resources")
require("map")
require("player")
require("enemies")
require("spike")
require("particles")
require("checkpoint")
require("jumppad")
require("orb")
require("coin")
require("menu")
require("levelselection")
require("data")

local host = require('hostinfo')
--crbase(os)

local love = love
local min = math.min
local max = math.max
local floor = math.floor
local lg = love.graphics
local last_setScale

TILEW = 16
WIDTH = 300
HEIGHT = 200

scale_x, scale_y =0,0
timeScale = 1	-- variable used for slow motion


STATE_MAINMENU = 0
STATE_INGAME_MENU = 1
STATE_INGAME = 2
STATE_LEVEL_MENU = 3
STATE_LEVEL_COMPLETED = 4

local SETSCALE_COOLDOWN = 0.1

function love.load()

 --require("loadAssets")
	--WebP = require("love-webp")			-- webp library for love
	 LastOrientation="portrait"
 MobileOrientation="portrait"
 SCREEN_WIDTH = 2160
 SCREEN_HEIGHT = 1440
 --love.window.setMode(SCREEN_WIDTH, SCREEN_HEIGHT, {resizable=true, borderless=false})
 

local success, error_message = pcall(function()
    joystick = love.joystick.getJoysticks()[1]
    require("gamepadinput")
    loadcontrollermappings()
    gamepadTimer = 0
end)

if not success then
    print("Error:", error_message)
end

	loadSettings()
	loadData()

	lg.setDefaultFilter("nearest","nearest")
	lg.setBackgroundColor(COLORS.darkbrown)
	lg.setLineStyle("rough")

	loadImages()
	
	loadSounds()
	loadAScentOfEurope()
	
	createQuads()
	createMenus()

	gamestate = STATE_MAINMENU
	current_menu = main_menu

	player = Player.create()
end

function love.update(dt)

updategetjoystickaxis(dt)
	isjoystickbeingpressed(joystick,button)

	if gamestate == STATE_INGAME then
		-- Upper bound on frame delay
		if dt > 0.06 then dt = 0.06 end

		if love.keyboard.isDown("s") then
			dt = dt/100
		end

		-- Progress timer
		map.time = map.time + dt

		-- Update entitites
		player:update(dt*timeScale)
		if player.x > MAPW+6 then
			gamestate = STATE_LEVEL_COMPLETED
		end

		Spike.globalUpdate(dt)
		Jumppad.globalUpdate(dt)
		Coin.globalUpdate(dt)

		-- Update enemies
		for i=#map.enemies,1,-1 do
			local enem = map.enemies[i]
			if enem.alive == true then
				if enem.update then
					enem:update(dt)
				end
			else
				table.remove(map.enemies, i)
			end
		end

		-- Update particles
		for i=#map.particles,1,-1 do
			local part = map.particles[i]
			if part.alive == true then
				part:update(dt)
			else
				table.remove(map.particles, i)
			end
		end

		-- Target screen translation
		local totx = player.x + 6.5 - WIDTH/2
		local toty = player.y + 10 - HEIGHT/2
		
		-- Calculate new screen translation
		if SCROLL_SPEED == 9 then
			tx = min(max(0, totx), MAPW-WIDTH)
			ty = min(max(0, toty), MAPH-HEIGHT)
		else
			--tx = min(max(0, tx+(totx-tx)*SCROLL_SPEED*dt), MAPW-WIDTH)
			--ty = min(max(0, ty+(toty-ty)*SCROLL_SPEED*dt), MAPH-HEIGHT)
			tx = min(max(0, tx + (totx - tx) * SCROLL_SPEED * dt * timeScale), MAPW - WIDTH)
            ty = min(max(0, ty + (toty - ty) * SCROLL_SPEED * dt * timeScale), MAPH - HEIGHT)
        
		end
	end
	
	   -- Call the update function of the credits menu if it is currently active
    if current_menu == credits_menu then
        credits_menu:update(dt)
    end
	
	checkOrientation()
end


	local zoomFactor = 1.0
	local timer = 0
	waterwarp = require 'waterwarp'	--warp effect intensity 10
function love.draw()

	
         if timeScale < 1 then
			zoomFactor = zoomFactor + (-myYSpeed) / 5000
			if zoomFactor<0.4 then zoomFactor=0.4 end
			lg.scale(scale_x/1.4+zoomFactor, scale_y+zoomFactor)
			love.graphics.translate(-50,-50)
			if (1/zoomFactor>0.1) then	-- prevent the game from crashing by checking that the pitch is not negative
				snd.Music:setPitch(1/zoomFactor) -- Adjust the speed factor as needed
			end
	
    else
        lg.scale(scale_x/1.4, scale_y)
        snd.Music:setPitch(1)
        zoomFactor=1
        
    end
	
	
		if MobileOrientation == "portrait" then
		love.graphics.rotate( 0 )
		love.graphics.translate(0,0)
	elseif MobileOrientation == "landscape" then
		--love.graphics.rotate( 1.57 )
		--love.graphics.translate(10,-200)
		love.graphics.translate(0,-10)
	end
	
		--love.graphics.rotate( 1.57 )
		--love.graphics.translate(0,-400)
	-- STATE: In game
	if gamestate == STATE_INGAME then
		lg.push()
		drawIngame()
		lg.pop()
		drawIngameHUD()
	elseif gamestate == STATE_INGAME_MENU then
		lg.push()
		drawIngame()
		lg.pop()
		current_menu:draw()
	elseif gamestate == STATE_MAINMENU then
	
		--love.graphics.draw(imgTitle, quads.title, 0,0, 0, WIDTH/900)
		--love.graphics.draw(imgTitle, 0,0, 0, WIDTH/1800)
		love.graphics.draw(imgTitleBck, 0,0, 0, WIDTH/1650)
		love.graphics.draw(imgTitleCat, 0,0, 0, WIDTH/1650)
		love.graphics.draw(imgTitleFrg, 0,0, 0, WIDTH/1650)
		love.graphics.draw(imgTitleTitle, 0,0, 0, WIDTH/1650)
		current_menu:draw()
			
		 -- Apply water warp shader only to imgTitleFrgWtr
		 timer=timer+0.05
		if timer>100 then timer=0 end
        love.graphics.setShader(warpshader)
        warpshader:send("time", timer)
        love.graphics.draw(imgTitleFrgWtr, 0, 0, 0, WIDTH / 1650)
        love.graphics.setShader() -- Reset the shader to default
								
		
	elseif gamestate == STATE_LEVEL_MENU then
		LevelSelection.draw()
	elseif gamestate == STATE_LEVEL_COMPLETED then
		lg.push()
		drawIngame()
		lg.pop()
		drawCompletionHUD()
	end
end

numImages=20

function drawIngame()
	if ASSETS == "new" then
	
	-- Draw the background layer with slower movement
	lg.translate(-tx/4, -ty/2)
	if current_map < 5 then
	
     -- Draw deeper layer of the background parallax image to cover the screen width
    for i = 1, numImages do
        local xOffset = (i - 1) * bck2Image:getWidth() * 0.18
        local xScale = 0.18
        if i % 2 == 0 then
            -- Invert the image on the x-axis for even iterations
            xScale = -0.18
            xOffset = xOffset + bck2Image:getWidth() * 0.18
        end
        
			love.graphics.draw(bck2Image, xOffset, 0, 0, xScale, 0.18)
		
    end
	end
	-- Compensate for accumulated translations before drawing the foreground layer
				lg.translate(tx/4, -ty/2)
	
	       -- Draw the Lamp layer with medium movement
		 	if current_map>4 then
				lg.translate(-tx/1, 0)
			else
				lg.translate(-tx/3, ty/2)
			end
    
if current_map > 4 then
       for i = 1, numImages do
            local xOffset = (i - 1) * templeWalls:getWidth() * 0.12 -- Adjust multiplier as needed
        local xScale = 0.18
        if i % 2 == 0 then
            -- Invert the image on the x-axis for even iterations
            xScale = -0.18
            --xOffset = xOffset + bck2Image:getWidth() * 0.18
        end    
        love.graphics.draw(templeWalls, xOffset, 80, 0, xScale, -0.18)
        love.graphics.draw(templeWalls, xOffset, 80, 0, xScale, 0.18)
    end
		
    else
    	
	-- Draw Lamp layer of the background parallax image to cover the screen width
    for i = 1, numImages do
        local xOffset = (i - 1) * bck2Lamp:getWidth() * 0.28
        local xScale = 0.18
        if i % 2 == 0 then
            -- Invert the image on the x-axis for even iterations
            xScale = -0.18
            xOffset = xOffset + bck2Lamp:getWidth() * 0.28
        end
        love.graphics.draw(bck2Lamp, xOffset,80, 0, xScale, 0.18)
    end
	
	end
	


    
    --compensate parallax
    	if current_map>4 then
			lg.translate(tx/1,  0)
    	else
			lg.translate(tx/3, -ty/2)
    	end

	if current_map > 4 then
		lg.translate(-tx/3, ty/2)
	else
		lg.translate(-tx/2, ty/2)
	end
	-- Draw Lamp layer of the background parallax image to cover the screen width
    local xOffset= 0
    for i = 1, numImages do
        if current_map > 4 then
			local xOffset = (i - 1) * imgFog:getWidth() * 0.18
        else
			local xOffset = (i - 1) * bckImage:getWidth() * 0.18
		end
        local xScale = 0.18
        if i % 2 == 0 then
            -- Invert the image on the x-axis for even iterations
            xScale = -0.18
            if current_map > 4 then
				xOffset = xOffset + imgFog:getWidth() * 0.18
            else
				xOffset = xOffset + bckImage:getWidth() * 0.18
            end
        end
        if current_map > 4 then
			--love.graphics.draw(imgFog, xOffset,80, 0, xScale, 0.18)
        else
			love.graphics.draw(bckImage, xOffset,180, 0, xScale, 0.18)
		end
    end
	
	 --compensate parallax	
	 if current_map > 4 then
		lg.translate(tx/3, -ty)
	 else
		lg.translate(tx/2, -ty)
	end
elseif ASSETS == "classic" then
	lg.translate(0, -ty*1.5)

	end
-- Draw the foreground layer with faster movement
	lg.translate(-tx, ty/2)
	
	 -- Draw background image
    --love.graphics.draw(bckImage, 0, 80, 0, 0.2,0.2)
    
     -- Calculate the number of background images needed to cover the screen width
    --local numImages = math.ceil(WIDTH / (bckImage:getWidth() * 0.2))
    

    -- Draw multiple instances of the background image to cover the screen width
    --for i = 1, numImages do
        --love.graphics.draw(bckImage, (i - 1) * bckImage:getWidth() * 0.2, 80, 0, 0.2, 0.2)
    --end
    

    
	map:setDrawRange(tx-20,ty,WIDTH+130,HEIGHT+20)
	map:draw()

	player:draw()

	for i,v in ipairs(map.entities) do
		v:draw()
	end

	for i,v in ipairs(map.coins) do
		v:draw()
	end

	for i,v in ipairs(map.enemies) do
		if v.draw then
			v:draw()
		end
	end

	for i,v in ipairs(map.particles) do
		v:draw()
	end
	
	
	
    --compensate parallax
    	if current_map>4 then
			lg.translate(tx, -ty/2)
    	else
			--lg.translate(tx/3, -ty/2)
    	end
    	
    			if current_map > 4 then
		lg.translate(-tx*1.5, -ty*1)
	else
		--lg.translate(-tx/2, ty/2)
	end
	
	-- Draw front Temple layer
    local xOffset= 0
    for i = 1, numImages do
        if current_map > 4 then
			local xOffset = (i - 1) * imgFrontTemple:getWidth() * 1
        else
			--local xOffset = (i - 1) * bckImage:getWidth() * 0.18
		end
        local xScale = 0.3
        if i % 2 == 0 then
            -- Invert the image on the x-axis for even iterations
            xScale = -0.3
            if current_map > 4 then
				xOffset = xOffset + imgFrontTemple:getWidth() * 1
            else
				--xOffset = xOffset + bckImage:getWidth() * 0.18
            end
        end
        if current_map > 4 then
			love.graphics.draw(imgFrontTemple, xOffset,120, 0, xScale, 0.3)
        else
			--love.graphics.draw(bckImage, xOffset,180, 0, xScale, 0.18)
		end
    end
	
	 --compensate parallax	
	 if current_map > 4 then
		lg.translate(tx*1.5, ty*1)
	 else
		--lg.translate(tx, -ty)
	end
	
	
	if current_map > 4 then
		lg.translate(-tx/3, ty/2)
	else
		--lg.translate(-tx/2, ty/2)
	end
	-- Draw front Temple fog
    local xOffset= 0
    for i = 1, numImages do
        if current_map > 4 then
			local xOffset = (i - 1) * imgFog:getWidth() * 1
        else
			--local xOffset = (i - 1) * bckImage:getWidth() * 0.18
		end
        local xScale = 1
        if i % 2 == 0 then
            -- Invert the image on the x-axis for even iterations
            xScale = -1
            if current_map > 4 then
				xOffset = xOffset + imgFog:getWidth() * 1
            else
				--xOffset = xOffset + bckImage:getWidth() * 0.18
            end
        end
        if current_map > 4 then
			love.graphics.draw(imgFog, xOffset,-30, 0, xScale, 0.18*1)
        else
			--love.graphics.draw(bckImage, xOffset,180, 0, xScale, 0.18)
		end
    end
	
	 --compensate parallax	
	 if current_map > 4 then
		lg.translate(tx/3, -ty)
	 else
		lg.translate(tx/2, -ty)
	end
	

	
end

function drawIngameHUD()
	local time = getTimerString(map.time)

	-- Draw text
	lg.draw(imgHUD, quads.hud_coin, 9, 10)
	lg.draw(imgHUD, quads.hud_skull, 48, 10)
	lg.setColor(1,1,1,1)
	lg.print(map.numcoins.."/5", 21, 13)
	lg.print(map.deaths, 67, 13)
	lg.printf(time, WIDTH-120, 13, 100, "right")
end

function drawCompletionHUD()
	lg.setColor(COLORS.menu)
	lg.rectangle("fill", 0,0, WIDTH,HEIGHT)
	lg.setColor(1,1,1,1)
	lg.draw(imgHUD, quads.text_level, 48,40)
	lg.draw(imgHUD, quads.text_cleared, 140,40)

	lg.print("COINS:", 66,75)
	lg.print(map.numcoins.."/5", 130,75)
	lg.print("DEATHS:", 66,95)
	lg.print(map.deaths, 130,95)
	lg.print("TIME:", 66,115)
	lg.print(getTimerString(map.time), 130,115)

	-- Draw coins difference
	if map.numcoins > level_status[current_map].coins then
		lg.setColor(COLORS.green)
		lg.print("+"..map.numcoins-level_status[current_map].coins, 160, 75)
	end
	-- Draw deaths difference
	if level_status[current_map].deaths then
		if map.deaths > level_status[current_map].deaths then
			lg.setColor(COLORS.red)
			lg.print("+"..map.deaths-level_status[current_map].deaths, 148, 95)
		elseif map.deaths < level_status[current_map].deaths then
			lg.setColor(COLORS.green)
			lg.print("-"..level_status[current_map].deaths-map.deaths, 148, 95)
		end
	end
	-- Draw time difference
	if level_status[current_map].time then
		if map.time > level_status[current_map].time then
			lg.setColor(COLORS.red)
			lg.print("+"..getTimerString(map.time-level_status[current_map].time), 194, 115)
		elseif map.time < level_status[current_map].time then
			lg.setColor(COLORS.green)
			lg.print("-"..getTimerString(level_status[current_map].time-map.time), 194, 115)
		end
	end


	lg.setColor(1,1,1,1)
	lg.print("PRESS ANY KEY TO CONTINUE", 55, 165)
end

function getTimerString(time)
	local msec = math.floor((time % 1)*100)
	local sec = math.floor(time % 60)
	local min = math.floor(time/60)
	return string.format("%02d'%02d\"%02d",min,sec,msec)
end

function love.keypressed(k)
	if gamestate == STATE_INGAME then
		if k == "space" or k == "z" or k == "x" then
			player:keypressed(k)
		elseif k == "escape" then
			gamestate = STATE_INGAME_MENU
			current_menu = ingame_menu
			ingame_menu.selected = 1
		elseif k == "r" then
			player:kill()
		elseif k == "return" then
			reloadMap()
		elseif k == "c" then
			gamestate = STATE_LEVEL_COMPLETED
		end
	elseif gamestate == STATE_INGAME_MENU or gamestate == STATE_MAINMENU then
		current_menu:keypressed(k)
	elseif gamestate == STATE_LEVEL_MENU then
		LevelSelection.keypressed(k)
	elseif gamestate == STATE_LEVEL_COMPLETED then
		levelCompleted()
	end
end

function love.keyreleased(k)
	if gamestate == STATE_INGAME then
		if k ~= "escape" and k ~= "r" then
			player:keyreleased(k)
		end
	end
end

local touches = {}

function love.touchpressed(id, x, y)
	table.insert(touches, {id = id, x = x, y = y})
	if gamestate == STATE_INGAME then
		player:keypressed(' ')
	end
end

function love.touchreleased(id, x, y)
	local ignore = false
	for i = #touches, 1, -1 do
		if touches[i].moved then
			ignore = true
		end
		if touches[i].id == id then
			table.remove(touches, i)
		end
	end
	if not ignore then
		if gamestate == STATE_INGAME then
			player:keyreleased(' ')
		elseif gamestate == STATE_INGAME_MENU or gamestate == STATE_MAINMENU then
			current_menu:keypressed('return')
		elseif gamestate == STATE_LEVEL_MENU then
			LevelSelection.keypressed('return')
		elseif gamestate == STATE_LEVEL_COMPLETED then
			levelCompleted()
		end
	end
end

function send(key)
				if gamestate == STATE_INGAME_MENU or gamestate == STATE_MAINMENU then
					current_menu:keypressed(key)
				elseif gamestate == STATE_LEVEL_MENU then
					LevelSelection.keypressed(key)
				end
			end

function love.touchmoved(id, x, y)
	for i, v in ipairs(touches) do
		if v.id == id and not v.moved then
			local xv = x - v.x
			local yv = y - v.y
			local axv = math.abs(xv)
			local ayv = math.abs(yv)

			-- Ignore touchmoves below a certain threshold
			local threshold = 0.08

			if axv < threshold and ayv < threshold then
				return
			end

			v.moved = true
			send(key)
			
			if MobileOrientation == "portrait" then
			
			if ayv > axv then
				if yv > 0 then
					send('down')
				elseif yv < 0 then
					send('up')
				end
			else
				if xv > 0 then
					send('right')
				elseif xv < 0 then
					send('left')
				end
			end
			elseif MobileOrientation == "landscape" then	-- invert swipe coordinates when in landscape mode
			 -- Swap axes for landscape mode
				if ayv > axv then
					if yv > 0 then
						send('right')  -- Swipe up becomes right
					elseif yv < 0 then
						send('left')   -- Swipe down becomes left
					end
				else
					if xv > 0 then
						send('up')   -- Swipe right becomes down
					elseif xv < 0 then
						send('down')     -- Swipe left becomes up
					end
				end
			end
			
			
			return
		end
	end
end

function love.focus(f)
	if f == false and gamestate == STATE_INGAME then
		gamestate = STATE_INGAME_MENU
		current_menu = ingame_menu
		ingame_menu.selected = 1
	end
end

function setScale(scale)
	if last_setScale and love.timer.getTime()-last_setScale < SETSCALE_COOLDOWN then return end
	if scale < 1 or scale == SCALE then return end

	last_setScale = love.timer.getTime()
	SCALE = scale

	if host.isTouchDevice() then
		scale_x = love.graphics.getWidth() / WIDTH
		scale_y = love.graphics.getHeight() / HEIGHT
	else
		scale_x = scale
		scale_y = scale
	end

	SCREEN_WIDTH  = WIDTH*SCALE
	SCREEN_HEIGHT = HEIGHT*SCALE
--[[
	love.window.setMode(SCREEN_WIDTH, SCREEN_HEIGHT, {
		fullscreen=false,
		vsync=true,
		borderless = host.isTouchDevice()
	})--]]
end

function setResolution(w,h)
	
	love.window.setMode(w, h, {
		fullscreen=false,
		vsync=true,
		borderless = host.isTouchDevice()
	})

	if w == 0 and h == 0 then
		SCREEN_WIDTH = lg.getWidth()
		SCREEN_HEIGHT = lg.getHeight()
		love.window.setMode(SCREEN_WIDTH, SCREEN_HEIGHT, {
			fullscreen=false,
			vsync=true,
			borderless = host.isTouchDevice()
		})
	else
		SCREEN_WIDTH = w
		SCREEN_HEIGHT = h
	end

	SCALE = 8
	while (20*16*SCALE) < SCREEN_WIDTH or (16*16*SCALE) < SCREEN_HEIGHT do
		SCALE = SCALE + 1
	end
	WIDTH = SCREEN_WIDTH/SCALE
	HEIGHT = SCREEN_HEIGHT/SCALE
end

function love.quit()
	saveSettings()
	saveData()
end

function checkOrientation()
	local desktopWidth, desktopHeight = love.window.getDesktopDimensions()
	
	--MobileOrientation="landscape"
		if desktopWidth<1100 then MobileOrientation="portrait"
			if LastOrientation==MobileOrientation then 

			else	
				LastOrientation=MobileOrientation
				scale_x = love.graphics.getWidth() / WIDTH
				scale_y = love.graphics.getHeight() / HEIGHT
				--love.window.setMode(2340, 1080, {resizable=true, borderless=false})
				
			end
	elseif desktopWidth>1100  then MobileOrientation="landscape"
			if LastOrientation==MobileOrientation then 

			else	
				LastOrientation=MobileOrientation
				scale_x = love.graphics.getWidth() / WIDTH
				scale_y = love.graphics.getHeight() / HEIGHT
				--love.window.setMode(1080, 2340, {resizable=true, borderless=false})
				
			end
	end
end
